# Cutting the image into 8x8 blocks

import numpy as np
from math import *


# Resize image for height and length multiple of 8


def resize(image):
    N = len(image)
    M = len(image[0])
    newN = N - N % 8
    newM = M - M % 8
    Mat = [[image[i][j] for j in range(newM)] for i in range(newN)]
    return(Mat)


# Cut into 8x8 blocks
#
# Input : frequency matrix
# Output : block matrix

def cutting_blocks(image):
    cropped = np.asarray(resize(image))  # resized image
    blocs = [[[] for j in range(len(image[0])//8)]
             for k in range(len(image)//8)]
    for i in range(len(image)//8):
        for j in range(len(image[0])//8):
            # block 8x8 of coord (i,j)
            bloc = cropped[i*8:(i+1)*8, j*8:(j+1)*8]
            blocs[i][j] = bloc.tolist()
    return(blocs)

# Assemble 8x8 blocks
#
# Input : block matrix
# Output : assembled matrix


def assemble_blocks(blocks, length, width):
    mat = [[blocks[(i//8)][(j//8)][i % 8][j % 8]
            for j in range(int(length)*8)] for i in range(int(width)*8)]
    return(mat)


# From list of blocks to matrix of blocks


def lst_to_mat(lst, longueur, largeur):
    Mat = [[lst[longueur*i + j]
            for j in range(longueur)] for i in range(largeur)]
    return(Mat)
