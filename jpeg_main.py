import quantification as qf
import subsample as se
import block_cutting as bc
import lumchro_converter as conv
import Huffman as huf
import rle_encoding as rle
import naive_cosine as dct
import zigzag_code as zz
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt


image_init = Image.open("cat.bmp")  # uncompressed format for an image of a cat
image = (np.asarray(image_init)).astype('float64')


masque = [[16, 11, 10, 16, 24, 40, 51, 61], [12, 12, 14, 19, 26, 58, 60, 55], [14, 13, 16, 24, 40, 57, 69, 56], [14, 17, 22, 29, 51, 87, 80, 62], [
    18, 22, 37, 56, 68, 109, 103, 77], [24, 35, 55, 64, 81, 104, 113, 92], [49, 64, 78, 87, 103, 121, 120, 101], [72, 92, 95, 98, 112, 100, 103, 99]]


masque_dequantif = masque


# Main function to convert an image to an encoded sequence (Huffman)

def jpeg_main(image):
    img_Y, img_Cb, img_Cr = conv.lumchro_tabl(bc.resize(image))
    img_echant_Cb = se.subsample420(bc.resize(img_Cb))
    img_echant_Cr = se.subsample420(bc.resize(img_Cr))
    img_blocs_Cb = bc.cutting_blocks(img_echant_Cb)
    img_blocs_Cr = bc.cutting_blocks(img_echant_Cr)
    img_blocs_Y = bc.cutting_blocks(img_Y)
    string_Y = str(len(img_blocs_Y[0])) + "£" + \
        str(len(img_blocs_Y)) + "£"  # for registering the number of blocks
    string_Cr = str(len(img_blocs_Y[0])) + "£" + \
        str(len(img_blocs_Y)) + "£"
    string_Cb = str(len(img_blocs_Y[0])) + "£" + \
        str(len(img_blocs_Y)) + "£"
    for i in range(len(img_blocs_Cb)):
        for j in range(len(img_blocs_Cb[0])):
            blocY = img_blocs_Y[i][j]  # luminance Y
            blocY = dct.cosine_transform(blocY)
            blocY = qf.quantif(blocY, masque)
            zigzagY = (zz.zigzag(blocY))
            string_Y += rle.rle(zigzagY)
            string_Y += "|"  # to separate each block

            blocCr = img_blocs_Cr[i][j]  # chrominance Cr
            blocCr = dct.cosine_transform(blocCr)
            blocCr = qf.quantif(blocCr, masque)
            zigzagCr = (zz.zigzag(blocCr))
            string_Cr += rle.rle(zigzagCr)
            string_Cr += "|"

            blocCb = img_blocs_Cb[i][j]  # chrominance Cb
            blocCb = dct.cosine_transform(blocCb)
            blocCb = qf.quantif(blocCb, masque)
            zigzagCb = (zz.zigzag(blocCb))
            string_Cb += rle.rle(zigzagCb)
            string_Cb += "|"

            # We get then the three RLE sequences
            RLEYseq = string_Y[:len(string_Y)-1]
            RLECb = string_Cb[:len(string_Cb)-1]
            RLECr = string_Cr[:len(string_Cr)-1]

            # We concatenate the three RLE sequences with %%% separating each component
            RLEseq = RLEYseq + "%%%" + RLECb + "%%%" + RLECr
            # We convert into Huffman format
            HuffmanEncoded, table = huf.fromRLE_to_Huffman(RLEseq)
    return(HuffmanEncoded, table)


# The function get_sequence allows
# from the string giving Huffman sequences separated by |
# to reform the list of sequences


def get_sequence(string):
    index = 0
    lst = []
    char = string[0]
    for k in range(len(string)):
        char = string[k]
        if char == "|":
            lst.append(string[index:k])
            index = k+1  # the new start for next sequence is reinitialised
    lst.append(string[index:len(string)])
    return(lst)


# Takes the full RLE sequence
# and give the three different components (Y,Cr,Cb)
# they are separated by %%%
def separate_components(rleseq):
    lst = []
    indexStart = 0  # we move a window to get the three sequences
    indexEnd = 0
    already_seen = False  # we have not seen any % at the start
    for k in range(len(rleseq)):
        char = rleseq[k]
        if char == "%":
            if not(already_seen):
                already_seen = True
                indexEnd = k
                lst.append(rleseq[indexStart:indexEnd])
                # and we reinitialise the indexes
                indexStart = indexEnd + 3  # because %%%
                indexEnd = indexStart
        else:  # there is no more %, so we reinitialise the boolean
            already_seen = False
    lst.append(rleseq[indexStart:])
    return(lst)

# From Huffman sequence allows to find back
# the initial image
# It is the main decompression function


def decompression_main(huffmanseq, table):
    decoded = huf.decode_huffman(huffmanseq, table)
    components = separate_components(decoded)
    rle_Y = components[0]
    rle_Cb = components[1]
    rle_Cr = components[2]
    return(decompression_fromRLE(rle_Y, rle_Cb, rle_Cr))

# Decompression from RLE sequence


def decompression_fromRLE(rle_Y, rle_Cb, rle_Cr):
    index = 0  # index to find the format of the sequence
    char = rle_Y[index]
    while char != "£":
        index += 1
        char = rle_Y[index]
    length = int(rle_Y[:index])  # the first string is the length
    indexEnd = index + 1
    char = rle_Y[indexEnd]
    while char != "£":
        indexEnd += 1
        char = rle_Y[indexEnd]
    width = int(rle_Y[index + 1: indexEnd])  # then we get the width
    # we then pass the info on format, already used
    rle_Y = rle_Y[indexEnd + 1:]
    rle_Cb = rle_Cb[indexEnd + 1:]
    rle_Cr = rle_Cr[indexEnd + 1:]
    lst_blocY = get_sequence(rle_Y)
    lst_blocCb = get_sequence(rle_Cb)
    lst_blocCr = get_sequence(rle_Cr)
    lstFinalY = []
    lstFinalCb = []
    lstFinalCr = []
    for k in range(len(lst_blocY)):
        # list of frequencies in zigzag order
        lstfreqY = rle.decode_rle(lst_blocY[k])
        lstfreqCb = rle.decode_rle(lst_blocCb[k])
        lstfreqCr = rle.decode_rle(lst_blocCr[k])
        # quantified frequencies matrices
        quantifiedY = zz.zigzag_reverse(lstfreqY)
        quantifiedCb = zz.zigzag_reverse(lstfreqCb)
        quantifiedCr = zz.zigzag_reverse(lstfreqCr)
    # we then find back the frequencies matrices before quantification
        blocFreqY = qf.reverse_quantif(quantifiedY, masque_dequantif)
        blocFreqCb = qf.reverse_quantif(quantifiedCb, masque_dequantif)
        blocFreqCr = qf.reverse_quantif(quantifiedCr, masque_dequantif)

    # We find by DCT reverse the spatial matrices
        blocY = dct.reverse_cosine_transform(blocFreqY)  # block of luminance
        blocCb = dct.reverse_cosine_transform(blocFreqCb)  # block of Cb
        blocCr = dct.reverse_cosine_transform(blocFreqCr)  # block of Cr

    # We appennd each block  in a list
        lstFinalY.append(blocY)
        lstFinalCb.append(blocCb)
        lstFinalCr.append(blocCr)

    # We use this list to reassemble the blocks

    # First, we put each block in a 4 dimensional matrix
    matBlocsY = bc.lst_to_mat(lstFinalY, length, width)
    matBlocsCb = bc.lst_to_mat(lstFinalCb, length, width)
    matBlocsCr = bc.lst_to_mat(lstFinalCr, length, width)

    # We merge to obtain finally a 2 dimensional matrix
    matY = bc.assemble_blocks(matBlocsY, length, width)
    matCb = bc.assemble_blocks(matBlocsCb, length, width)
    matCr = bc.assemble_blocks(matBlocsCr, length, width)
    matRVB = conv.versRVB(matY, matCb, matCr)
    return(matRVB)


HuffmanEncoded, table = jpeg_main(image)


# This function allows to write the sequence in a text file

def write_text(file, text):
    f = open(file, 'w')
    f.write(text)
    f.close()


print("The size in bytes is " + str(len(HuffmanEncoded)))

plt.figure()
plt.imshow(image_init)
plt.title("Original")

plt.figure()
plt.imshow(decompression_main(HuffmanEncoded, table))
plt.title("Compressed")

plt.show()
