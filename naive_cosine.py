# Naive algorithm of discrete cosine transform (DCT)

from math import *

from numpy import matrix

# Characteristic coefficient of DCT


def C(x):
    if x == 0:
        return(1/sqrt(2))
    else:
        return(1)

# DCT of a coordinate (i,j) of the spatial matrix


def cosine(tableau, i, j):
    somme = 0
    N = len(tableau)
    M = len(tableau[0])
    for x in range(N):
        for y in range(M):
            somme += tableau[x][y] * cos((2*x + 1)*i *
                                         pi / (2*N)) * cos((2*y + 1)*j*pi / (2*M))
    somme *= 2 * C(i) * C(j) / N
    return(somme)

# DCT
#
# Input : spatial
# Output : frequency matrix by DCT


def cosine_transform(tableau):
    N = len(tableau)
    M = len(tableau[0])
    transformed = [[0 for k in range(M)] for i in range(N)]
    for i in range(N):
        for j in range(M):
            transformed[i][j] = (cosine(tableau, i, j))
    return(transformed)

# Inverse DCT of a coordinate (x,y) of frequency matrix


def reverse_cosine(tableau, x, y):
    somme = 0
    N = len(tableau)
    M = len(tableau[0])
    for i in range(N):
        for j in range(M):
            somme += C(i) * C(j) * tableau[i][j] * cos((2*x + 1)*i *
                                                       pi / (2*N)) * cos((2*y + 1)*j*pi / (2*M))
    somme *= 2 / N
    return(somme)

# Inverse DCT
#
# Input : frequency matrix
# Output : spatial matrix with inverse DCT


def reverse_cosine_transform(tableau):  # inverse cosine transform
    N = len(tableau)
    M = len(tableau[0])
    # original spatial matrix
    reverse = [[0 for k in range(M)] for i in range(N)]
    for x in range(N):
        for y in range(M):
            reverse[x][y] = round(reverse_cosine(tableau, x, y))
    return(reverse)
