import numpy as np
import scipy.signal as si

# https://www.researchgate.net/publication/242178213_On_the_JPEG2000_Implementation_on_Different_Computer_Platforms
# 0 , ±1 , ±2 , ...
DAUBECHIES_97_ANALYSIS_LO = np.array([0.6029490182363579,0.2668641184428723,-0.07822326652898785,-0.01686411844287495,0.02674875741080976])
DAUBECHIES_97_ANALYSIS_HI = np.array([1.115087052456994,-0.5912717631142470,-0.05754352622849957,0.09127176311424948])

DAUBECHIES_97_FILTER_LO = np.array([1.115087052456994,0.5912717631142470,-0.05754352622849957,-0.09127176311424948])
DAUBECHIES_97_FILTER_HI = np.array([0.6029490182363579,-0.2668641184428723,-0.07822326652898785,0.01686411844287495,0.02674875741080976])

DAUBECHIES_97_WAVELET = [DAUBECHIES_97_ANALYSIS_LO,DAUBECHIES_97_ANALYSIS_HI,DAUBECHIES_97_FILTER_LO,DAUBECHIES_97_FILTER_HI]


LEGALL_53_ANALYSIS_LO = np.array([6/8,2/8,-1/8])
LEGALL_53_ANALYSIS_HI = np.array([1,-1/2])

LEGALL_53_FILTER_LO = np.array([1,1/2])
LEGALL_53_FILTER_HI = np.array([6/8,-2/8,-1/8])

LEGALL_97_WAVELET = [LEGALL_53_ANALYSIS_LO,LEGALL_53_ANALYSIS_HI,LEGALL_53_FILTER_LO,LEGALL_53_FILTER_HI]

# --------------------------- #

def test(f,input,expected_value,equality_test = lambda x,y : x==y):
    if equality_test(f(input),expected_value):
        print("Test Passed - ",f.__name__)
    else :
        raise RuntimeError("TEST FAILED - ",f.__name__)




# --- Wavelet-related functions

def filter_from_wavelet(wavelet):
    """[a[0],a[±1],a[±2],...] -> [...,a[-1],a[0],a[1],...]"""
    return np.hstack([wavelet[::-1],wavelet[1:]])

def periodic_symmetric_convolution(data,filter):
    """
        Performs convolution on data with periodic extension to account for border effects
        
        Requires :
            |filter| < |data|
            |filter| is odd
    """
    N = data.shape[0]
    extended_data = np.hstack([data,data,data])

    extended_filtered = np.convolve(extended_data,filter,mode='same')

    return extended_filtered[N:2*N]



def single_step_1D_decomposition(array,wavelet):
    pass

def single_step_2D_decomposition(array,wavelet):
    pass




# --- Tests

if __name__ == "__main__":

    test(filter_from_wavelet,np.array([1,2,3]),np.array([3,2,1,2,3]),np.array_equal)

    test(periodic_convolution(np.array([1,2,3,2,1]),np.array([1,1,1])),np.array([]))
