# Zigzag function right before RLE encoding
#
# Input : frequency matrix 8x8
# Output : list in zigzag order


def zigzag(Mat):
    if len(Mat) != 8 or len(Mat[0]) != 8:
        print("Incoherent size")
    else:
        L = [0 for k in range(64)]  # initialisation
        cmpt = 0
        for diag in range(8):  # top left side, the 8 first diagonals
            if diag % 2 == 0:  # if the diagonal is even, from bottom left to top right
                for k in range(diag + 1):
                    L[cmpt] = Mat[diag-k][k]
                    cmpt += 1
            else:  # if diagonal is odd, from top right to bottom left
                for k in range(diag + 1):
                    L[cmpt] = Mat[k][diag-k]
                    cmpt += 1
        for diag in range(7):  # bottom right side, the last 7 diagonals
            if diag % 2 == 0:
                for k in range(7-diag):
                    L[cmpt] = Mat[7-k][diag + k+1]
                    cmpt += 1
            else:
                for k in range(7 - diag):
                    L[cmpt] = Mat[diag + k+1][7 - k]
                    cmpt += 1
    return(L)


# Function zigzag_reverse
#
# Input : a list with zigzag order
# Output : corresponding frequency matrix


def zigzag_reverse(L):
    if len(L) != 64:
        print("Incoherent length")
    else:
        Mat = [[0 for j in range(8)] for i in range(8)]
        cmpt = 0
        for diag in range(8):  # top left side, the 8 first diagonals
            if diag % 2 == 0:  # if the diagonal is even, from bottom left to top right
                for k in range(diag + 1):
                    Mat[diag-k][k] = L[cmpt]
                    cmpt += 1
            else:  # if diagonal is odd, from top right to bottom left
                for k in range(diag + 1):
                    Mat[k][diag-k] = L[cmpt]
                    cmpt += 1
        for diag in range(7):  # bottom right side, the last 7 diagonals
            if diag % 2 == 0:
                for k in range(7-diag):
                    Mat[7-k][diag + k+1] = L[cmpt]
                    cmpt += 1
            else:
                for k in range(7 - diag):
                    Mat[diag + k+1][7 - k] = L[cmpt]
                    cmpt += 1
        return(Mat)
