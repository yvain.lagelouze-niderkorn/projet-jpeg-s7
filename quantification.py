import naive_cosine as dct


# Quantification of F by the mask Q
#
# Input : a frequency matrix (obtained by DCT) and a mask Q
# Output : new quantified matrix, high frequencies are cleared out by the mask

def quantif(F, Q):
    new = F.copy()
    for i in range(len(new)):
        for j in range(len(new[0])):
            new[i][j] = round(F[i][j]/Q[i][j])
    return(new)


# Inverse quantification
#
# Input : quantified matrix
# Output : original matrix (not exactly the original because of lossy quantification)

def reverse_quantif(quantified, Q):
    Fapp = quantified.copy()
    for i in range(len(Fapp)):
        for j in range(len(Fapp[0])):
            Fapp[i][j] = quantified[i][j] * Q[i][j]
    return(Fapp)
